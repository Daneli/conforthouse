﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.LexicoCompilador;

namespace Manejador.LexicoCompilador
{
    public class ManejadorExpresionesLexico
    {
        string patronCadenas = @"string [a-z]=['[:ascii:]*']";
        string patronPalabrareservada = @"Movimiento|Sensor";
        string patronComparacion = @"\b[a-z]\w*>[0-9]\d*|[a-z]\w*<[0-9]\d*|[a-z]\w*>=[0-9]\d*|[a-z]\w*<=[0-9]\d*|[a-z]\w*==[0-9]\d*|[a-z]\w*!=[0-9]\d*\b";
        string patronIf = @"^if[([:ascii:]*)]";
        string patronInstancia = @"[A-Za-z]* [a-z] = new [A-Za-z]*[(:ascii:)*]";
        string patronFuncion = @"m[(\d*,\w*\s*)]|t[(\d*,\w*\s*)]|g[(\d*,\w*\s*)]";
        string patronResultado = @"[A-Za-z]*=[(\w*\s*)]";
        string patronLlaveA = @"\173";
        string patronLlaveC = @"\175";
        string patronMain = @"\b^Main()";
        string patronInicio = @"\bInicio";
        string patronElse = @"\b[^else([:ascii:]*})]\b";
        string patronFin = @"\bFinalizar";
        string patronComentarios = @"[\047\047[A-Z-a-z\w*]";
        public List<Tokens> Separar(string texto)
        {
            List<Tokens> r = new List<Tokens>();

            string[] palabra = texto.Split('\n');

            for (int i = 0; i < palabra.Length; i++)
            {
                if (!palabra[i].Equals(" "))
                {
                    if (ComprobarLexico(palabra[i], patronCadenas))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "Cadena", i + 1));
                    }
                    else if(ComprobarLexico(palabra[i], patronPalabrareservada))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "Palabra Reservada", i + 1));
                    }
                    else if (ComprobarLexico(palabra[i], patronInstancia))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "Instancia", i + 1));
                    }
                    else if(ComprobarLexico(palabra[i], patronComparacion))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "Comparacion", i + 1));
                    }
                    else if (ComprobarLexico(palabra[i], patronIf))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "If", i + 1));
                    }
                    else if (ComprobarLexico(palabra[i], patronLlaveA))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "Llave inicial", i + 1));
                    }
                    else if (ComprobarLexico(palabra[i], patronResultado))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "Resultado", i + 1));
                    }
                   
                    else if (ComprobarLexico(palabra[i], patronLlaveC))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "Llave final", i + 1));
                    }
                    else if(ComprobarLexico(palabra[i], patronInicio))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "Iniciar", i + 1));
                    }
                    else if(ComprobarLexico(palabra[i], patronFin))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "Fin", i + 1));
                    }
                    else if (ComprobarLexico(palabra[i], patronMain))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "Main", i + 1));
                    }
                    else if (ComprobarLexico(palabra[i], patronFuncion))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "Funcion", i + 1));
                    }
                    else if (ComprobarLexico(palabra[i], patronElse))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "Else", i + 1));
                    }
                    else if (ComprobarLexico(palabra[i], patronComentarios))
                    {
                        r.Add(new Tokens(i + 1, palabra[i], "Comentario", i + 1));
                    }
                    
                    else
                    {
                        r.Add(new Tokens(i+1,palabra[i], "No identificado", i + 1));
                    }
                }
            }
            return r;
        }
        public bool ComprobarLexico(string palabra, string patron)
        {
            Match comprobacion = Regex.Match(palabra, patron);
            bool resultado = false;
            if(comprobacion.Success)
            {
                resultado = true;
            }
            return resultado;
        }
    }
}
