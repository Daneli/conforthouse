﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Manejador.LexicoCompilador
{
    public class ManejadorSemantico
    {
      
        //Linea 1 debe existir un Iniciar
        public string ReglaUno(DataGridView tabla)
        {
            string r = "";
            for (int i = 0; i < tabla.RowCount; i++)
            {
                if (tabla.Rows[i].Cells[2].Value.ToString().Equals("Iniciar"))
                {
                    if (tabla.Rows[i + 1].Cells[2].Value.ToString().Equals("Instancia"))
                    {
                        r = "Correcto";
                        i++;
                    }
                    else
                    {
                        r = "Error semantico 1: Despues de la linea Iniciar va una Instancia";
                        break;
                    }
                }
            }
            return r;
        }
        //En la ultima linea debe existir un Finalizar
        public string ReglaDos(DataGridView tabla)
        {
            string r = "";
            if (tabla.Rows[tabla.RowCount - 1].Cells[1].Value.ToString().Equals("Finalizar"))
            {
                r = "";
            }
            else
                r = "Error semantico 2: la última línea debe ser Finalizar";
            return r;
        }
        // Debe existir un Main despues de una instancia
        public string ReglaTres(DataGridView tabla)
        {
            string r = "";
            for (int i = 0; i < tabla.RowCount; i++)
            {
                if (tabla.Rows[i].Cells[2].Value.ToString().Equals("Instancia"))
                {
                    if (tabla.Rows[i + 1].Cells[2].Value.ToString().Equals("Main"))
                    {
                        r = "Correcto";
                        i++;
                    }
                    else
                    {
                        r = "Error semantico 3: Despues de la linea Instancia va un Main";
                        break;
                    }
                }
            }
            return r;
        }
        //Despues de un Main va una Funcion
        public string ReglaCuatro(DataGridView tabla)
        {
            string r = "";
            for (int i = 0; i < tabla.RowCount; i++)
            {
                if (tabla.Rows[i].Cells[2].Value.ToString().Equals("Main"))
                {
                    if (tabla.Rows[i + 1].Cells[2].Value.ToString().Equals("Funcion"))
                    {
                        r = "Correcto";
                        i++;
                    }
                    else
                    {
                        r = "Error semantico 4: Despues de un Main va una funcion";
                        break;
                    }
                }
            }
            return r;
        }
        //Solo existen dos palabras reservadas
        public string ReglaCinco(DataGridView tabla)
        {
            string r = "";
            for (int i = 0; i < tabla.RowCount; i++)
            {
                if (tabla.Rows[i].Cells[2].Value.ToString().Equals("Funcion"))
                {
                    if (tabla.Rows[i + 1].Cells[2].Value.ToString().Equals("Llave inicial"))
                    {
                        r = "Correcto";
                        i++;
                    }
                    else
                    {
                        r = "Error semantico 5: Despues de una Funcion va una Llave Final";
                        break;
                    }
                }
            }
            return r;
        }
        //Analisis Semantico-----------------------------------------------------------------------------------------
        public string Semantico(DataGridView tabla)
        {
            string r = "";
            if (tabla.RowCount > 0)
            {
                r = ReglaUno(tabla);
                if (r.Length == 0) 
                    r = ReglaDos(tabla);
                if (r.Length == 0)
                    r = ReglaTres(tabla);
                if (r.Length == 0)
                    r = ReglaCuatro(tabla);
                if (r.Length == 0)
                    r = ReglaCinco(tabla);
            }
            return r;
        } 
    }
}
