﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Manejador.LexicoCompilador
{
    public class ManejadorSintactico
    {
        //Inicio----------------------------------------------------------------------------------------------
        public string Inicio(DataGridView tabla)
        {
            string r = " ";

            if (tabla.Rows[0].Cells[2].Value.ToString().Equals("Iniciar"))
            {
                r = "";
            }
            else
            {
                r = "Error sintactico 1: Linea Inicio incorrecta";
            }
            return r;
        }
        //Instancias--------------------------------------------------------------------------------------------
        
       
        //Instancias temperatura, gas y movimiento
        public string Instancias(DataGridView tabla)
        {
            string r = "";
            for (int i = 0; i < tabla.RowCount; i++)
            {
                string descripcion = tabla.Rows[i].Cells[2].Value.ToString();
                if (descripcion == "Instancia")
                {
                    string token = tabla.Rows[i].Cells[1].Value.ToString();
                    string[] arreglo = token.Split(' ');
                    if (arreglo[0].Equals("Actuarmovimiento") && arreglo[1].Equals("m"))
                    {
                        
                        if (arreglo[4].Equals("Actuarmovimiento"))
                        {
                            string parametro = arreglo[5];
                            string[] puerto = parametro.Split('(',')');
                            string c = parametro.Substring(0, 1);
                            string a = parametro.Substring(parametro.Length - 2, 1);
                            if(c.Equals("(") && a.Equals(")"))
                            {
                                if(int.Parse(puerto[1])>=2 && int.Parse(puerto[1])<=13)
                                {
                                    r = "";
                                }
                                else
                                {
                                    r = "Error 4 (Instancia):Puerto incorrecto debe ser mayor o igual a dos y menor o igual a 13.";
                                }
                            }
                            else
                            {
                                r = "Error 5 (Instancia): Hacen fatla parentesis ()";
                            }
                           
                        }
                        else
                        {
                            r = "Error sintactico 3 (Instancia ): Las instancias deben de terminar en Actuargas (puerto), Actuartemperatura (puerto) o Actuarmovimiento (puerto)";
                        }
                    }
                    else if (arreglo[0].Equals("Actuartemperatura") && arreglo[1].Equals("t"))
                    {
                        if (arreglo[4].Equals("Actuartemperatura"))
                        {
                            string parametro = arreglo[5];
                            string[] puerto = parametro.Split('(',')');
                            string c = parametro.Substring(0, 1);
                            string a = parametro.Substring(parametro.Length - 2, 1);
                            if (c.Equals("(") && a.Equals(")"))
                            {
                                if (int.Parse(puerto[1]) >= 2 && int.Parse(puerto[1]) <= 13)
                                {
                                    r = "";
                                }
                                else
                                {
                                    r = "Error 4 (Instancia):Puerto incorrecto debe ser mayor o igual a dos y menor o igual a 13.";
                                }
                            }
                            else
                            {
                                r = "Error 5 (Instancia): Hacen fatla parentesis ()";
                            }

                        }
                        else
                        {
                            r = "Error sintactico 3 (Instancia ): Las instancias deben de terminar en Actuargas (puerto), Actuartemperatura (puerto) o Actuarmovimiento (puerto)";
                        }
                    }
                    else if (arreglo[0].Equals("Actuargas") && arreglo[1].Equals("g"))
                    {
                        if (arreglo[4].Equals("Actuargas"))
                        {
                            string parametro = arreglo[5];
                            string[] puerto = parametro.Split('(', ')');
                            string c = parametro.Substring(0, 1);
                            string a = parametro.Substring(parametro.Length - 2, 1);
                            if (c.Equals("(") && a.Equals(")"))
                            {
                                if (int.Parse(puerto[1]) >= 2 && int.Parse(puerto[1]) <= 13)
                                {
                                    r = "";
                                }
                                else
                                {
                                    r = "Error sintactico 5 (Instancia):Puerto incorrecto debe ser mayor o igual a dos y menor o igual a 13.";
                                }
                            }
                            else
                            {
                                r = "Error sintactico 4 (Instancia): Hacen fatla parentesis ()";
                            }

                        }
                        else
                        {
                            r = "Error sintactico 3 (Instancia): Las instancias deben de terminar en Actuargas (puerto), Actuartemperatura (puerto) o Actuarmovimiento (puerto)";
                        }
                    }
                    else
                    {
                        r = "Error sintactico 2 (Instancia): Las instancias deben de ser Actuargas g, Actuartemperatura t, Actuarmovimiento m";
                    }
                }
            }
            return r;
        }

        //Main-----------------------------------------------------------------------------------------------------------
        public string Main(DataGridView tabla)
        {
            string r = " ";
            for (int i = 0; i < tabla.RowCount; i++)
            {
                string descripcion = tabla.Rows[i].Cells[2].Value.ToString();
                if (descripcion.Equals("Main"))
                {
                    string token = tabla.Rows[i].Cells[1].Value.ToString();
                    string pi = token.Substring(4, 1);
                    string pu = token.Substring(token.Length-2, 1);
                    if(pi.Equals("(") && pu.Equals(")"))
                    {
                        r = "";
                        i = tabla.RowCount + 1;
                    }
                    else
                    {
                        r = "Error sintactico 6 (Main): Mein incorrecto";
                    }
                }
            }
            return r;
        }

        //Funcion-------------------------------------------------------------------------------------------------------

        public string Funcion(DataGridView tabla)
        {
            string r = "";
            for (int i = 0; i < tabla.RowCount; i++)
            {
                string descripcion = tabla.Rows[i].Cells[2].Value.ToString();
                if (descripcion == "Funcion")
                {
                    string token = tabla.Rows[i].Cells[1].Value.ToString();
                    string[] arreglo = token.Split(' ');
                    if (arreglo[0].Equals("m"))
                    {
                        string parametro = arreglo[1];
                        string[] puerto = parametro.Split('(',',');
                        string p1 = parametro.Substring(0, 1);
                        string p2 = parametro.Substring(parametro.Length - 1, 1);
                        if (p1.Equals("(") && p2.Equals(","))
                        {
                            if (int.Parse(puerto[1]) >= 2 && int.Parse(puerto[1]) <= 13)
                            {
                                string parametrodos = arreglo[2];
                                string[] puertodos = parametrodos.Split(')');
                                string p3 = parametrodos.Substring(parametrodos.Length - 2, 1);
                                if (p3.Equals(")"))
                                {
                                    if (puertodos.Equals("Encender"))
                                    {
                                        r = "";
                                    }
                                }
                                else
                                {
                                    r = "Error sintactico 10 (Funcion): Debe ser Encender, Apagar, Abrir o Cerrar";
                                }
                            }
                            else
                            {
                                r = "Error sintactico 9 (Funcion): Los puertos deben ser mayor o igual a 2 y menor o igual a 13.";
                            }
                        }
                        else
                        {
                            r = "Error sintactico 8 (Funcion): Hacen fatla parentesis ( y/o una coma";
                        }
                    }
                    else if (arreglo[0].Equals("t"))
                    {
                        string parametro = arreglo[1];
                        string[] puerto = parametro.Split('(', ',');
                        string p1 = parametro.Substring(0, 1);
                        string p2 = parametro.Substring(parametro.Length - 1, 1);
                        if (p1.Equals("(") && p2.Equals(","))
                        {
                            if (int.Parse(puerto[1]) >= 2 && int.Parse(puerto[1]) <= 13)
                            {
                                string parametrodos = arreglo[2];
                                string[] puertodos = parametrodos.Split(')');
                                string p3 = parametrodos.Substring(parametrodos.Length - 2, 1);
                                if (p3.Equals(")"))
                                {
                                    if (puertodos.Equals("Encender"))
                                    {
                                        r = "";
                                    }
                                }
                                else
                                {
                                    r = "Error sintactico 10 (Funcion): Debe ser Encender, Apagar, Abrir o Cerrar";
                                }
                            }
                            else
                            {
                                r = "Error sintactico 9 (Funcion): Los puertos deben ser mayor o igual a 2 y menor o igual a 13.";
                            }
                        }
                        else
                        {
                            r = "Error sintactico 8 (Funcion): Hacen fatla parentesis ( y/o una coma";
                        }
                    }
                    else if (arreglo[0].Equals("g"))
                    {
                        string parametro = arreglo[1];
                        string[] puerto = parametro.Split('(', ',');
                        string p1 = parametro.Substring(0, 1);
                        string p2 = parametro.Substring(parametro.Length - 1, 1);
                        if (p1.Equals("(") && p2.Equals(","))
                        {
                            if (int.Parse(puerto[1]) >= 2 && int.Parse(puerto[1]) <= 13)
                            {
                                string parametrodos = arreglo[2];
                                string[] puertodos = parametrodos.Split(')');
                                string p3 = parametrodos.Substring(parametrodos.Length - 2, 1);
                                if (p3.Equals(")"))
                                {
                                    if (puertodos.Equals("Encender"))
                                    {
                                        r = "";
                                    }
                                }
                                else
                                {
                                    r = "Error sintactico 10 (Funcion): Debe ser Encender, Apagar, Abrir o Cerrar";
                                }
                            }
                            else
                            {
                                r = "Error sintactico 9 (Funcion): Los puertos deben ser mayor o igual a 2 y menor o igual a 13.";
                            }
                        }
                        else
                        {
                            r = "Error sintactico 8 (Funcion): Hacen fatla parentesis ( y/o una coma";
                        }
                    }
                    else
                    {
                        r = "Error sintactico 7 (Funcion): La funciones deben comenzar con m(espacio),t(espacio) o g(espacio)";
                    }
                }
            }
            return r;
        }
       

        //Llave inicial-----------------------------------------------------------------------------------------------------------
        public string Llaveinicio(DataGridView tabla)
        {
            string r = " ";
            for (int i = 0; i < tabla.RowCount; i++)
            {
                string descripcion = tabla.Rows[i].Cells[2].Value.ToString();
                if (descripcion.Equals("Llave inicial"))
                {
                    string token = tabla.Rows[i].Cells[1].Value.ToString();
                    string l = token.Substring(0, 1);
                    if (l.Equals("{"))
                    {
                        r = "";
                        i = tabla.RowCount + 1;
                    }
                    else
                    {
                        r = "Error sintactico 18 (Llave inicial): Falta una llave inicial";
                    }
                }
            }
            return r;
        }

        //Llave final-----------------------------------------------------------------------------------------------------------
        public string Llavefinal(DataGridView tabla)
        {
            string r = " ";
            for (int i = 0; i < tabla.RowCount; i++)
            {
                string descripcion = tabla.Rows[i].Cells[2].Value.ToString();
                if (descripcion.Equals("Llave final"))
                {
                    string token = tabla.Rows[i].Cells[1].Value.ToString();
                    string l = token.Substring(0, 1);
                    if (l.Equals("}"))
                    {
                        r = "";
                        i = tabla.RowCount + 1;
                    }
                    else
                    {
                        r = "Error sintactico 19 (Llave final): Falta una llave final";
                    }
                }
            }
            return r;
        }

        //Mensaje-------------------------------------------------------------------------------------------------------
        public string Mensaje(DataGridView tabla)
        {
            string r = "";
            for (int i = 0; i < tabla.RowCount; i++)
            {
                string descripcion = tabla.Rows[i].Cells[2].Value.ToString();
                if (descripcion.Equals("Resultado"))
                {
                    string token = tabla.Rows[i].Cells[1].Value.ToString();
                    string[] arreglo = token.Split(' ','=');
                    if (arreglo[0].Equals("m"))
                    {
                        string parametro = arreglo[1];
                        string[] puerto = parametro.Split('(', ':');
                        string p1 = parametro.Substring(0, 1);
                        string p2 = parametro.Substring(parametro.Length - 1, 1);
                        if (p1.Equals("(") && p2.Equals(":"))
                        {
                            if (puerto[1].Equals("Encender"))
                            {
                                string parametrodos = arreglo[2];
                                string[] puertodos = parametrodos.Split(')');
                                string p3 = parametrodos.Substring(parametrodos.Length - 2, 1);
                                if (p3.Equals(")"))
                                {
                                    if (int.Parse(puertodos[0]) >= 5 && int.Parse(puertodos[0]) <= 30)
                                    {
                                        r = "";
                                    }
                                    else
                                    {
                                        r = "Error sintactico 23 (Mensaje): Tiempo debe ser mayor/igual a 5 segundos y menor/igual a 30 segundos";
                                    }
                                }
                                else
                                {
                                    r = "Error sintactico 22 (Mensaje): Hce falta un parentesis al final del tiempo";
                                }
                            }
                                
                            else
                            {
                                r = "Error sintactico 21 (Mensaje): Deben ser Encender";
                            }
                        }
                        else
                        {
                            r = "Error sintactico 20 (Mensaje): Hacen falta un parentesis al inicio  y dos puntos, por ejemplo (Encender:";
                        }
                    }
                    else if (arreglo[0].Equals("t"))
                    {
                        string parametro = arreglo[1];
                        string[] puerto = parametro.Split('(', ':');
                        string p1 = parametro.Substring(0, 1);
                        string p2 = parametro.Substring(parametro.Length - 1, 1);
                        if (p1.Equals("(") && p2.Equals(":"))
                        {
                            if (puerto[1].Equals("Encender"))
                            {
                                string parametrodos = arreglo[2];
                                string[] puertodos = parametrodos.Split(')');
                                string p3 = parametrodos.Substring(parametrodos.Length - 2, 1);
                                if (p3.Equals(")"))
                                {
                                    if (int.Parse(puertodos[0]) >= 5 && int.Parse(puertodos[0]) <= 30)
                                    {
                                        r = "";
                                    }
                                    else
                                    {
                                        r = "Error sintactico 24 (Mensaje): Tiempo debe ser mayor/igual a 5 segundos y menor/igual a 30 segundos";
                                    }
                                }
                                else
                                {
                                    r = "Error sintactico 25 (Mensaje): Hce falta un parentesis al final del tiempo";
                                }
                            }

                            else
                            {
                                r = "Error sintactico 26 (Mensaje): Deben ser Encender";
                            }
                        }
                        else
                        {
                            r = "Error sintactico 27 (Mensaje): Hacen falta un parentesis al inicio  y dos puntos, por ejemplo (Encender:";
                        }
                    }
                    else if (arreglo[0].Equals("g"))
                    {
                        string parametro = arreglo[1];
                        string[] puerto = parametro.Split('(', ':');
                        string p1 = parametro.Substring(0, 1);
                        string p2 = parametro.Substring(parametro.Length - 1, 1);
                        if (p1.Equals("(") && p2.Equals(":"))
                        {
                            if (puerto[1].Equals("Encender"))
                            {
                                string parametrodos = arreglo[2];
                                string[] puertodos = parametrodos.Split(')');
                                string p3 = parametrodos.Substring(parametrodos.Length - 2, 1);
                                if (p3.Equals(")"))
                                {
                                    if (int.Parse(puertodos[0]) >= 5 && int.Parse(puertodos[0]) <= 30)
                                    {
                                        r = "";
                                    }
                                    else
                                    {
                                        r = "Error sintactico 28 (Mensaje): Tiempo debe ser mayor/igual a 5 segundos y menor/igual a 30 segundos";
                                    }
                                }
                                else
                                {
                                    r = "Error sintactico 29 (Mensaje): Hce falta un parentesis al final del tiempo";
                                }
                            }

                            else
                            {
                                r = "Error sintactico 30 (Mensaje): Deben ser Encender";
                            }
                        }
                        else
                        {
                            r = "Error sintactico 31 (Mensaje): Hacen falta un parentesis al inicio  y dos puntos, por ejemplo (Encender:";
                        }
                    }

                    else
                    {
                        r = "Error sintactico 32 (Mensaje): Es un resultado debe ser m=(Encender: 15),t=(Encender: 15),g=(Encender: 15),";
                    }
                }
            }
            return r;
        
        }

        //Else-------------------------------------------------------------------------------------------------------
        public string Else(DataGridView tabla)
        {
            string r = "";
            for (int i = 0; i < tabla.RowCount; i++)
            {
                string descripcion = tabla.Rows[i].Cells[2].Value.ToString();
                if (descripcion == "Else")
                {
                    string token = tabla.Rows[i].Cells[1].Value.ToString();
                    string[] arreglo = token.Split('(', ')');
                    if (arreglo[0].Equals("else") && arreglo[1].Equals("No Encender"))
                    {
                        r = "";
                    }
                    else if (arreglo[0].Equals("else") && arreglo[1].Equals("No Abrir"))
                    {
                        r = "";
                    }

                    else
                    {
                        r = "Error sintactico 8: else solo va else(No Encender) o esle(No abrir)";
                    }
                }
            }
            return r;
        }

        //Analisis Sintactico--------------------------------------------------------------------------------------------------
        public string AnalisisSintactico(DataGridView tabla)
        {
            string r = "";
            if (tabla.RowCount > 1)
            {
                r = Inicio(tabla);
                if (r.Length == 0)
                    r = Instancias(tabla);
                if (r.Length == 0)
                    r = Main(tabla);
                if (r.Length == 0)
                    r = Funcion(tabla);
                if (r.Length == 0)
                    r = Llaveinicio(tabla);
                if (r.Length == 0)
                    r = Mensaje(tabla);
                if (r.Length == 0)
                    r = Llavefinal(tabla);
                if (r.Length == 0)
                    r = Else(tabla);
            }

            else
            {
                MessageBox.Show("Editor de codigo vacio: Debes escribir en ele editor para poder ejecutar");
            }


            return r;
        }
            //   
            //        
            //        
            //        //Arbol If
            //        else if (tabla.Rows[i].Cells[1].Value.ToString().Equals("if(p=actuar)\r") ||
            //                    tabla.Rows[i].Cells[1].Value.ToString().Equals("if(v=actuar)\r") ||
            //                    tabla.Rows[i].Cells[1].Value.ToString().Equals("if(f=actuar)\r") ||
            //                    tabla.Rows[i].Cells[1].Value.ToString().Equals("if(g>detectar)\r") ||
            //                    tabla.Rows[i].Cells[1].Value.ToString().Equals("if(a=actuar)\r") ||
            //                    tabla.Rows[i].Cells[1].Value.ToString().Equals("if(f>actuarmas)\r") ||
            //                    tabla.Rows[i].Cells[1].Value.ToString().Equals("if(t=actuar)\r") ||
            //                    tabla.Rows[i].Cells[1].Value.ToString().Equals("if(f<actuarmenos)\r") ||
            //                    tabla.Rows[i].Cells[1].Value.ToString().Equals("if(t>actuar)\r") ||
            //                    tabla.Rows[i].Cells[1].Value.ToString().Equals("if(t<actuar)\r"))
            //             {
            //                if (tabla.Rows[i].Cells[2].Value.ToString().Equals("If"))
            //                {
            //                    if (tabla.Rows[i + 1].Cells[2].Value.ToString().Equals("Llave inicial"))
            //                        r = "Correcto";
            //                    else
            //                    {
            //                        r = "Error hace falta una llave inicial depues de un If en la linea:"+" "+(7);
            //                        break;
            //                    }
            //                }
            //             }

                 
    }
}
