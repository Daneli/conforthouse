﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.LexicoCompilador;
using Manejador.LexicoCompilador;

namespace Presentacion.LexicoCompilador
{
    public partial class btnSintactico : Form
    {
        List<Tokens> Listapalabras;
        ManejadorExpresionesLexico ml;
        ManejadorSintactico ms;
        ManejadorSemantico msp;
        
        public btnSintactico()
        {
            InitializeComponent();
            Listapalabras = new List<Tokens>();
            ml = new ManejadorExpresionesLexico();
            ms = new ManejadorSintactico();
            msp = new ManejadorSemantico();
        }
        private void btnsalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Lexico_Load(object sender, EventArgs e)
        {

        }
        private void btnEjecutar_Click(object sender, EventArgs e)
        {
            if (CompilarLexico())
                AnalisisSintactico();
                 AnalisisSemantico();
        }
        bool CompilarLexico()
        {
            dtglexicos.Columns.Clear();
            Listapalabras = ml.Separar(txtlexico.Text);
            dtglexicos.DataSource = Listapalabras.ToList();
            dtglexicos.AutoResizeColumns();
            if (dtglexicos.RowCount > 0)
                return true;
            else
                return false;
        }
        void AnalisisSintactico()
        {
            string r = ms.AnalisisSintactico(dtglexicos);
            if (r.Length > 0)
            {
                MessageBox.Show(r);
            }
        }

        void AnalisisSemantico()
        {
            string r = msp.Semantico(dtglexicos);
            if (r.Length > 0)
                MessageBox.Show(r);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (StreamWriter outfile = new StreamWriter("C:\\Users\\DaneliReyes\\Desktop\\Conforthouse\\conforthouse\\ConfortHome_Lexico\\Presentacion.LexicoCompilador\\bin\\Debug\\archivo.txt"))
            {
                outfile.WriteLine(txttraducir);
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
