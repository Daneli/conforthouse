﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.LexicoCompilador
{
    public class Tokens
    {
        public int Numero { get; set; }
        public string Token { get; set; }
        public string Descripcion { get; set; }
        public int NumeroLinea { get; set; }

        public Tokens(int numero, string token, string descripcion, int numeroLinea)
        {
            Numero = numero;
            Token = token;
            Descripcion = descripcion;
            NumeroLinea = numeroLinea;
        }
    }
}
